import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:ntp/ntp.dart';
import 'package:vatch/drawer_screens/getRegion.dart';
import 'package:vatch/network/repository/my_repository.dart';
import 'package:vatch/utils/choose_data.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'homeBody/watch.dart';
import 'homeBody/weather.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _value = 1;
  bool _isDayMode = true;
  bool _isLoading = true;
  static String region = 'Tashkent';
  static String regionName = 'Toshkent';
  String _temprature = "...";
  String _time = "...";
  String _date = "...";
  String _weekday = "...";
  AssetImage _selectWeatherImage;
  TextEditingController controller = new TextEditingController();
  MyRepository _apiProvider = new MyRepository();
  final _scaffoldkey = new GlobalKey<ScaffoldState>();
  bool _isPortait = true;

  @override
  void initState() {
    super.initState();
    getTime();
    pullFromApi(region);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      backgroundColor:
          _isDayMode ? Colors.white : Color.fromARGB(100, 0, 26, 72),
      body: OrientationBuilder(builder: (context, orientation) {
        print(orientation);
        if (orientation == Orientation.portrait) {
          _isPortait = true;
          return getBodyPotrait();
        } else {
          _isPortait = false;
          return getBodyLandscape();
        }
        setState(() {});
      }),
      // getBody(),
      floatingActionButton: floatButton(),
      endDrawer: getDrawer(),
    );
  }

  Widget getBodyPotrait() {
    return Container(
      padding: EdgeInsets.only(top: 40),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: _isDayMode
                  ? AssetImage('assets/images/background.png')
                  : AssetImage('assets/images/background_light.png'),
              fit: BoxFit.cover)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          watch(_time, _isDayMode, _date, _weekday, _isPortait),
          SizedBox(height: 20),
          ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/weather/heavy_sleet.png'),
                      ),
                    ),
                  ),
                ),
                wether(_temprature, _isDayMode, regionName),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getBodyLandscape() {
    return Container(
      padding: EdgeInsets.only(top: 40, bottom: 40),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: _isDayMode
                  ? AssetImage('assets/images/background.png')
                  : AssetImage('assets/images/background_light.png'),
              fit: BoxFit.cover)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ///////////\\\\\\\\\\\
          watch(_time, _isDayMode, _date, _weekday, _isPortait),

          SizedBox(width: 24),

          ///////////\\\\\\\\\\\
          ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/weather/heavy_sleet.png'),
                      ),
                    ),
                  ),
                ),
                wether(_temprature, _isDayMode, regionName),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getDrawer() {
    return Container(
      width: 200,
      color: _isDayMode ? Colors.white : Color.fromARGB(100, 0, 26, 72),
      child: Drawer(
        child: Padding(
          padding: EdgeInsets.fromLTRB(8, 50, 8, 0),
          child: Column(
            children: <Widget>[
              LiteRollingSwitch(
                value: _isDayMode,
                textOff: "night",
                textOn: "day",
                colorOff: Colors.yellow[600],
                colorOn: Colors.blueGrey,
                iconOff: Icons.nights_stay,
                iconOn: Icons.wb_sunny,
                onChanged: (bool position) {
                  position
                      ? Get.changeTheme(ThemeData.light())
                      : Get.changeTheme(ThemeData.dark());
                  print("isDay = $position");
                },
              ),
              DropdownButton(
                value: _value,
                dropdownColor: Colors.blueGrey,
                // iconDisabledColor: Colors.red,
                // iconEnabledColor: Colors.red,
                // iconSize: 24,
                items: [
                  DropdownMenuItem(
                    child: Text('Toshkent'),
                    value: 1,
                  ),
                  DropdownMenuItem(
                    child: Text('Nukus'),
                    value: 2,
                  ),
                  DropdownMenuItem(
                    child: Text('Urganch'),
                    value: 3,
                  ),
                  DropdownMenuItem(
                    child: Text('Samarqand'),
                    value: 4,
                  ),
                  DropdownMenuItem(
                    child: Text('Jizzax'),
                    value: 5,
                  ),
                  DropdownMenuItem(
                    child: Text('Guliston'),
                    value: 6,
                  ),
                  DropdownMenuItem(
                    child: Text('Navoiy'),
                    value: 7,
                  ),
                  DropdownMenuItem(
                    child: Text('Qarshi'),
                    value: 8,
                  ),
                  DropdownMenuItem(
                    child: Text('Termiz'),
                    value: 9,
                  ),
                  DropdownMenuItem(
                    child: Text('Buxoro'),
                    value: 10,
                  ),
                  DropdownMenuItem(
                    child: Text('Farg`ona'),
                    value: 11,
                  ),
                  DropdownMenuItem(
                    child: Text('Andijon'),
                    value: 12,
                  ),
                  DropdownMenuItem(
                    child: Text('Namangan'),
                    value: 13,
                  ),
                ],
                onChanged: (int value) {
                  if (mounted) {
                    setState(() {
                      _value = value;
                      region = getRegion(value);
                      regionName = getRegionName(value);
                      pullFromApi(region);
                    });
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget floatButton() {
    return FloatingActionButton(
      onPressed: () => _scaffoldkey.currentState.openEndDrawer(),
      tooltip: 'Increment',
      child: Icon(Icons.settings_sharp),
    );
  }

  Future getTime() async {
    Timer.periodic(Duration(milliseconds: 1000), (timer) async {
      try {
        // DateTime _novTime = await NTP.now();
        // if (mounted) {
        // }
        setTime(await NTP.now());
      } catch (e) {
        setTime(DateTime.now());
      }
    });
  }

  void setTime(DateTime _novTime) {
    setState(() {
      _time = "${_novTime.hour}:" + getRealMinute(_novTime.minute);
      _date = "${_novTime.day} - " + getMonth(_novTime.month);
      _weekday = getWeekDay(_novTime.weekday);
      _isDayMode = (_novTime.hour > 7 && _novTime.hour < 19) ? true : false;
    });
  }

  void pullFromApi(String region) {
    // Timer.periodic(Duration(minutes: 30), (timer) {
    setIsLoading(true);
    _apiProvider
        .getWeatherData(region)
        .then((value) {
          print(value.city.title);
          double temp = double.parse(value.airT.toString());
          if (mounted) {
            setState(() {
              if (temp > 0)
                _temprature = "+$temp";
              else
                _temprature = "$temp";

              _selectWeatherImage = selectWeatherImage(value.weatherCode);
            });
          }
          print(
              "////////////////\n$_temprature\n$_date\n$_weekday\n////////////////");
        })
        .catchError((onError) => {
              print(onError.toString()),
            })
        .whenComplete(() => {setIsLoading(false)});
    // });
  }

  setIsLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }
}
