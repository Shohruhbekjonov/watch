import 'package:flutter/material.dart';
import 'package:vatch/utils/choose_data.dart';

Widget wether(String temp, bool isDay, String regionName) {
  return Expanded(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          temp,
          style: TextStyle(
              fontSize: 60,
              color: isDay ? Colors.black : Colors.white),
        ),
        Text(
          regionName,
          style: TextStyle(
              fontSize: 30,
              color: isDay ? Colors.black : Colors.white),
        ),
      ],
    ),
  );

}