import 'package:flutter/material.dart';

Widget watch(String _time, bool _isDayMode, String _date, String _weekday,
    bool orientPortrait) {
  return Container(

    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: Colors.blue, width: 2)),
    child: Stack(
      alignment: AlignmentDirectional.center,
      fit: StackFit.loose,
      children: <Widget>[
        Padding(
          padding: orientPortrait
              ? EdgeInsets.fromLTRB(60, 100, 60, 50)
              : EdgeInsets.only(top: 40, left: 100, right: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _time,
                style: TextStyle(
                  fontSize: 65,
                  fontWeight: FontWeight.bold,
                  color: _isDayMode ? Colors.blue : Colors.white,
                ),
              ),
              Text(
                _date,
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  color: _isDayMode ? Colors.blueGrey : Colors.white,
                ),
              ),
              Text(
                _weekday,
                style: TextStyle(
                  fontSize: 40,
                  color: _isDayMode ? Colors.blueGrey : Colors.white,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: -80,
          child: Container(
            alignment: Alignment.center,
            height: 160,
            width: 120,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/gerb.png'),
              ),
            ),
          ),
        ),
      ],
      overflow: Overflow.visible,
    ),
  );
}
