import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'home.dart';
import 'package:flutter/services.dart';
import 'package:wakelock/wakelock.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      // statusBarColor: Colors.transparent
      statusBarIconBrightness: Brightness.dark));
  runApp(MyApp());
  HttpOverrides.global = new MyHttpOverrides();
  Wakelock.enabled;
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan,
        // scaffoldBackgroundColor: Colors.white,

        fontFamily: 'Georgia',

        textTheme: TextTheme(
          bodyText1: TextStyle(fontSize: 14, fontFamily: "Hind"),
          headline1: TextStyle(fontSize: 72, fontWeight: FontWeight.bold),
          headline2: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      // darkTheme: ThemeData(
      //   brightness: Brightness.dark,
      //   primaryColor: Colors.lightBlue[800],
      //   accentColor: Colors.cyan,
      //   scaffoldBackgroundColor: Colors.blueGrey,
      //   fontFamily: 'Georgia',
      //
      //   textTheme: TextTheme(
      //     bodyText1: TextStyle(fontSize: 14, fontFamily: "Hind"),
      //     headline1: TextStyle(fontSize: 72, fontWeight: FontWeight.bold),
      //     headline2: TextStyle(fontStyle: FontStyle.italic),
      //   ),
      // ),
      // // themeMode: ThemeData.dark,
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
