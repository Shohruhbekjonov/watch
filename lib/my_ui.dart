import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MYUi extends StatefulWidget {
  @override
  _MYUiState createState() => _MYUiState();
}

class _MYUiState extends State<MYUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Container(
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 32, right: 32),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(color: Colors.blue, width: 2),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(32),
                    child: Row(
                      children: [
                        Expanded(
                          child: Image.asset('assets/weather/mostly_clear.png'),
                          flex: 3,
                        ),
                        SizedBox(width: 16,),
                        Expanded(
                          flex: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "+27",
                                style: TextStyle(
                                  fontSize: 55,
                                ),
                              ),
                              Text(
                                "Toshkent",
                                style: TextStyle(
                                  fontSize: 30,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 16),
                child: AspectRatio(
                  aspectRatio: 2.5,
                  child: Image.asset(
                    "assets/images/gerb.png",
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
