import 'dart:convert';

import 'package:http/http.dart';
import 'package:vatch/network/models/weather_model.dart';
import 'package:vatch/utils/exeptions.dart';

class MyRepository {
  static const headers = {"Content-Type": "application/json"};
  String baseUrl = "http://my2.dev.gov.uz/azamat/ru/weather/get-details?region";

  Client client = Client();

  Future<WeatherResponseModel> getWeatherData(String city) async {
    var responseJson;
    try {
      final response = await client.get(
          "$baseUrl=$city",
          headers: headers);

      var res = _response(response);
      print(response.request.headers.toString());

      responseJson = WeatherResponseModel.fromJson(res);
      print('$responseJson');
    } on FetchDataException {
      throw FetchDataException("No Internet connection");
    }
    return responseJson;
  }

  dynamic _response(Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        print("401 error");
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
