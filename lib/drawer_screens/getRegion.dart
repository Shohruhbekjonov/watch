import 'package:flutter/material.dart';

String getRegion (int value) {
  switch (value) {
    case 1:
      return 'Tashkent';
      break;
    case 2:
      return 'Nukus';
      break;
    case 3:
      return 'Urgench';
      break;
    case 4:
      return 'Samarkand';
      break;
    case 5:
      return 'Jizzakh';
      break;
    case 6:
      return 'Gulistan';
      break;
    case 7:
      return 'Navoiy';
      break;
    case 8:
      return 'Qarshi';
      break;
    case 9:
      return 'Termez';
      break;
    case 10:
      return 'Bukhara';
      break;
    case 11:
      return 'Fergana';
      break;
    case 12:
      return 'Andijan';
      break;
    case 13:
      return 'Namangan';
      break;
  }
}

String getRegionName (int value) {
  switch (value) {
    case 1:
      return 'Toshkent';
      break;
    case 2:
      return 'Nukus';
      break;
    case 3:
      return 'Urganch';
      break;
    case 4:
      return 'Samarqand';
      break;
    case 5:
      return 'Jizzax';
      break;
    case 6:
      return 'Guliston';
      break;
    case 7:
      return 'Navoiy';
      break;
    case 8:
      return 'Qarshi';
      break;
    case 9:
      return 'Termiz';
      break;
    case 10:
      return 'Buxoro';
      break;
    case 11:
      return 'Farg`ona';
      break;
    case 12:
      return 'Andijon';
      break;
    case 13:
      return 'Namangan';
      break;
  }
}