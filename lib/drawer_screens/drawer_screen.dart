import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  String region = "Tashkent";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      child: Drawer(
        child: Padding(
          padding: EdgeInsets.fromLTRB(40, 50, 50, 0),
          child: Column(
            children: <Widget>[
              DropdownButton(
                value: 1,
                items: [
                  DropdownMenuItem(
                    child: Text('Toshkent'),
                    value: 1,
                  ),
                  DropdownMenuItem(
                    child: Text('Nukus'),
                    value: 2,
                  ),
                  DropdownMenuItem(
                    child: Text('Urganch'),
                    value: 3,
                  ),
                  DropdownMenuItem(
                    child: Text('Samarqand'),
                    value: 4,
                  ),
                  DropdownMenuItem(
                    child: Text('Jizzax'),
                    value: 5,
                  ),
                  DropdownMenuItem(
                    child: Text('Guliston'),
                    value: 6,
                  ),
                  DropdownMenuItem(
                    child: Text('Navoiy'),
                    value: 7,
                  ),
                  DropdownMenuItem(
                    child: Text('Qarshi'),
                    value: 8,
                  ),
                  DropdownMenuItem(
                    child: Text('Termiz'),
                    value: 9,
                  ),
                  DropdownMenuItem(
                    child: Text('Buxoro'),
                    value: 10,
                  ),
                  DropdownMenuItem(
                    child: Text('Farg`ona'),
                    value: 11,
                  ),
                  DropdownMenuItem(
                    child: Text('Andijon'),
                    value: 12,
                  ),
                  DropdownMenuItem(
                    child: Text('Namangan'),
                    value: 13,
                  ),
                ],
                onChanged: (int value) {
                  setState(
                    () {
                      switch (value) {
                        case 1:
                          region = 'Tashkent';
                          break;
                        case 2:
                          region = 'Nukus';
                          break;
                        case 3:
                          region = 'Urgench';
                          break;
                        case 4:
                          region = 'Samarkand';
                          break;
                        case 5:
                          region = 'Fergana';
                          break;
                        case 6:
                          region = 'Andijan';
                          break;
                        case 7:
                          region = 'Namangan';
                          break;
                        case 8:
                          region = 'Gulistan';
                          break;
                        case 9:
                          region = 'Jizzakh';
                          break;
                        case 10:
                          region = 'Qarshi';
                          break;
                        case 11:
                          region = 'Termez';
                          break;
                        case 12:
                          region = 'Bukhara';
                          break;
                        case 13:
                          region = 'Navoiy';
                          break;
                      }
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
